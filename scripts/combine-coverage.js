const libCoverage = require("istanbul-lib-coverage");
const { createReporter } = require("istanbul-api");

const integrationCoverage = require("../combine-report/coverage-final-integration.json");
const unitCoverage = require("../combine-report/coverage-final-unit.json");

const normalizeJestCoverage = (obj) => {
  const result = obj;
  Object.entries(result).forEach(([k, v]) => {
    if (v.data) result[k] = v.data;
  });
  return result;
};

const map = libCoverage.createCoverageMap();
map.merge(normalizeJestCoverage(integrationCoverage));
map.merge(normalizeJestCoverage(unitCoverage));

const reporter = createReporter();
reporter.addAll(["json", "lcov", "text"]);
reporter.write(map);

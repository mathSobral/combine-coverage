const fs = require("fs");
const path = require("path");

const args = require("minimist")(process.argv.slice(2));

const TEST_FOLDER = `./test/${args.folder}`;

function getSpecFiles() {
  const allSpecFiles = walk(TEST_FOLDER);

  return allSpecFiles.sort();
}

function walk(dir) {
  let files = fs.readdirSync(dir);
  files = files.map((file) => {
    const filePath = path.join(dir, file);
    const stats = fs.statSync(filePath);
    if (stats.isDirectory()) return walk(filePath);
    else if (stats.isFile()) return filePath;
  });

  return files.reduce((all, folderContents) => all.concat(folderContents), []);
}

console.log(getSpecFiles().join(" "));

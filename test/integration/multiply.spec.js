import { multiply } from "../../src/mutiply";

describe("multiply", () => {
  test("Should multiply two values", () => {
    const result = multiply(2, 2);
    expect(result).toBe(4);
  });
});

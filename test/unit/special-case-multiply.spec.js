import { multiply } from "../../src/mutiply";

describe("Special Case Multiply", () => {
  test("Should test multiply special use case", () => {
    const result = multiply(10, 2);
    expect(result).toBe(0);
  });
});

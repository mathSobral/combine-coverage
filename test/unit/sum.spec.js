import { sum } from "../../src/sum";

describe("sum", () => {
  test("Should sum two values", () => {
    const result = sum(1, 1);
    expect(result).toBe(2);
  });
});
